import XCTest
@testable import ImageExportErrorExample
import simd

final class ImageExportErrorExampleTests: XCTestCase {
    func testConvertFloatArrayToPixelBufferAndCorrectReadValues() throws {
        let width = 192
        let height = 256
        let pattern = makePattern(width: width, height: height)

        // Create depth buffer, that we would receive from ARKit
        guard let buffer = try makeDepthBuffer(from: pattern, width: width, height: height) else {
            XCTFail("Failed to create test pattern")
            return
        }

        // Check if initial pattern equals created CVImageBuffer
        for index in 0..<pattern.count {
            let x = index % width
            let y = index / width

            let bufferValue = try buffer.getValue(x: x, y: y, for: Float.self)
            XCTAssertEqual(bufferValue, pattern[index])
        }
    }

    func testWriteDepthFloat32ReimportItAndHaveMinimalDifference() throws {
        let width = 192
        let height = 256
        let pattern = makePattern(width: width, height: height, maxValue: 1.0)

        // Create depth buffer, that we would receive from ARKit
        guard let buffer = try makeDepthBuffer(from: pattern, width: width, height: height) else {
            XCTFail("Failed to create test pattern")
            return
        }

        // Export Buffer
        let url = FileManager.default
            .temporaryDirectory
            .appendingPathComponent("depth\(UUID().uuidString).png")
        let exporter = PNGExporter()
        try exporter.writeGrayPixelBuffer(buffer: buffer, format: .bitSize16, url: url)

        // Re-Import Buffer
        let importer = PNGImporter()
        let importedBuffer = try importer.importGrayPixelBuffer(url: url, format: .bitSize16)

        // by converting from 32 to 16 bits, we lose precision
        // this is the maximum error, that can occur during conversion
        let accuracy = 1.0 / Float(UInt16.max)

        // Check if initial pattern equals imported CVImageBuffer
        for index in 0..<pattern.count {
            let x = index % width
            let y = index / width

            let rawBufferValue: UInt16 = try importedBuffer.getValue(x: x, y: y, for: UInt16.self)
            let bufferValue: Float = Float(rawBufferValue) / Float(UInt16.max)
            XCTAssertEqual(bufferValue, pattern[index], accuracy: accuracy)
        }
    }
}

// MARK: - Helpers
extension ImageExportErrorExampleTests {
    /// creates a depth buffer, that looks like the image created by ARKit.
    func makeDepthBuffer(from values: [Float], width: Int, height: Int) throws -> CVImageBuffer? {
        var bytes = values
        let pixelFormat = kCVPixelFormatType_DepthFloat32
        var buffer: CVPixelBuffer?
        CVPixelBufferCreateWithBytes( nil, width, height, pixelFormat, &bytes, width * 4, nil, nil, nil, &buffer)
        return try buffer?.deepcopy()
    }

    /// creates a gradient array of values.
    func makePattern(width: Int, height: Int, maxValue: Float = 20) -> [Float] {
        let numItems = Float(width * height)
        return (0..<width * height).map { (Float($0) / numItems) * maxValue }
    }
}

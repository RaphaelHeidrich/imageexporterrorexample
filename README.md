# ImageExportErrorExample

We noticed different behavior, when writing PNGs, on iOS 15 and iOS 16. This repository is a minimum example of this difference.

The test `ImageExportErrorExampleTests.testWriteDepthFloat32ReimportItAndHaveMinimalDifference` fails only on iOS 16 simulators.

## What the test does
- creates a `Float` array with values between `0` and `1`.
- creates a `CVPixelBuffer` containing these values
    - we use `kCVPixelFormatType_DepthFloat32` for it
- write a PNG file using `PNGExporter`
- read the very same PNG file using `PNGImporter`
- convert `UInt16` pixel value to `Float`
- compare `Float` pixel values with `Float` array
- must be approximately equal
- difference must not be bigger than 32-to-16bit precision loss

## It's most likely the export

We used the `PNGExporter` to export a unique pattern as a 16-bit gray value image. This was done under both iOS 15 and iOS 16. 
  
Naive expectation: They produce the same image  
Reality: They produce slightly different files

By using [imagemagick compare](https://imagemagick.org/script/compare.php), we were able to tell the difference.

### Conclusion: the error is introduced by a difference in PNG export

## What we noticed

Since we suspect the export method, we compared a bunch of involved framework helpers. We noticed, that `CIContext` has a different default declaration in iOS 16.


iOS 15.5
```
<CIContext: 0x600000668000 (metal 1) MTLDevice=0x15ad234a0>
    cache intermediates: yes
    priority: default
    workingSpace: <CGColorSpace 0x60000206d3e0> (kCGColorSpaceICCBased; kCGColorSpaceModelRGB; sRGB Linear)
    workingFormat: RGBAh
    downsampleQuality: Low
```

iOS 16.1
```
<CIContext: 0x6000005e0000 (metal 1) MTLDevice=0x14c304910>
    cacheIntermediates: yes
    priority: default
    workingSpace: "sRGB Linear"
    workingFormat: RGBAh
    downsampleQuality: Low
    maxRenderLoad: 256 MB
```

Since it's heavily involved in the export process, this might be a good hint.

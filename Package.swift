// swift-tools-version: 5.6

import PackageDescription

let package = Package(
    name: "ImageExportErrorExample",
    platforms: [
        .iOS(.v15)
    ],
    products: [
        .library(
            name: "ImageExportErrorExample",
            targets: ["ImageExportErrorExample"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "ImageExportErrorExample",
            dependencies: []),
        .testTarget(
            name: "ImageExportErrorExampleTests",
            dependencies: ["ImageExportErrorExample"]),
    ]
)

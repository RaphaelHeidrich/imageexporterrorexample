import CoreVideo

extension CVImageBuffer {
    public func deepcopy() throws -> CVPixelBuffer {
        let width = CVPixelBufferGetWidth(self)
        let height = CVPixelBufferGetHeight(self)
        let format = CVPixelBufferGetPixelFormatType(self)
        let attachments = CVBufferCopyAttachments(self, CVAttachmentMode.shouldPropagate)
        var pixelBufferCopyOptional: CVPixelBuffer?

        let result = CVPixelBufferCreate(nil, width, height, format, attachments, &pixelBufferCopyOptional)
        guard result == kCVReturnSuccess, let pixelBufferCopy = pixelBufferCopyOptional else {
            throw CVError("Failed to create CVPixelBuffer", code: result)
        }

        try self.lock(.readOnly)
        try pixelBufferCopy.lock([])

        let planeCount = CVPixelBufferGetPlaneCount(self)

        if planeCount > 1 {
            for plane in 0..<planeCount {
                let dest = CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, plane)
                let source = CVPixelBufferGetBaseAddressOfPlane(self, plane)
                let height = CVPixelBufferGetHeightOfPlane(self, plane)
                let bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(self, plane)

                memcpy(dest, source, height * bytesPerRow)
            }
        } else {
            let dataSize = CVPixelBufferGetDataSize(self)
            let baseAddress = CVPixelBufferGetBaseAddress(self)
            let target = CVPixelBufferGetBaseAddress(pixelBufferCopy)

            memcpy(target, baseAddress, dataSize)
        }

        try pixelBufferCopy.unlock([])
        try self.unlock(.readOnly)

        return pixelBufferCopy
    }

    public func lock(_ lockFlags: CVPixelBufferLockFlags) throws {
        let result = CVPixelBufferLockBaseAddress(self, lockFlags)
        if result != kCVReturnSuccess {
            throw CVError("Failed to lock pixel buffer base address", code: result)
        }
    }

    public func unlock(_ lockFlags: CVPixelBufferLockFlags) throws {
        let result = CVPixelBufferUnlockBaseAddress(self, lockFlags)
        if result != kCVReturnSuccess {
            throw CVError("Failed to unlock pixel buffer base address", code: result)
        }
    }

    public func getValue<T>(x: Int, y: Int, for type: T.Type) throws -> T {
        try self.lock(.readOnly)

        guard let baseAddress = CVPixelBufferGetBaseAddress(self) else {
            throw CVError("Cannot obtain base address for pixel buffer")
        }

        let buffer = baseAddress.assumingMemoryBound(to: type)
        let width = CVPixelBufferGetWidth(self)
        let value = buffer[x + y * width]

        try self.unlock(.readOnly)

        return value
    }
}

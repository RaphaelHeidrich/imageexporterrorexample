import CoreImage

class PNGExporter {
    public let ciContext: CIContext

    init(ciContext: CIContext = .init()) {
        self.ciContext = ciContext
    }

    public func writeGrayPixelBuffer(
        buffer: CVPixelBuffer,
        format: MonochromeFormat,
        url: URL
    ) throws {
        let imageCI = CIImage(cvPixelBuffer: buffer)

        guard let colorSpace = imageCI.colorSpace else {
            throw CVError("The image’s color space cannot be determined.")
        }

        var ciFormat: CIFormat
        switch format {
        case .bitSize16:
            ciFormat = .L16
        case .bitSize8:
            ciFormat = .L8
        }

        try ciContext.writePNGRepresentation(
            of: imageCI,
            to: url,
            format: ciFormat,
            colorSpace: colorSpace,
            options: [:]
        )
    }
}

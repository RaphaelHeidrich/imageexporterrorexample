import CoreVideo

class PNGImporter {
    func importGrayPixelBuffer(
        url: URL,
        format: MonochromeFormat
    ) throws -> CVPixelBuffer {
        guard
            let provider = CGDataProvider(url: url as CFURL),
            let image = CGImage(
                pngDataProviderSource: provider,
                decode: nil,
                shouldInterpolate: false,
                intent: .defaultIntent
            ),
            let colorSpace = image.colorSpace
        else {
            throw CVError("Unable to read from \(url.absoluteString)")
        }

        guard let pixelBuffer = image.pixelBufferGray(
            width: image.width,
            height: image.height,
            pixelFormatType: format == .bitSize8 ? kCVPixelFormatType_OneComponent8 : kCVPixelFormatType_OneComponent16,
            colorSpace: colorSpace,
            bitmapInfo: image.bitmapInfo
        ) else {
            throw CVError("Cannot convert to CVPixelBuffer")
        }
        
        return pixelBuffer
    }
}

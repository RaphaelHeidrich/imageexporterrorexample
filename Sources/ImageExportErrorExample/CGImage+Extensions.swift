import CoreGraphics
import CoreVideo

extension CGImage {
    public func pixelBufferGray(
        width: Int,
        height: Int,
        pixelFormatType: OSType = kCVPixelFormatType_OneComponent8,
        colorSpace: CGColorSpace = CGColorSpaceCreateDeviceGray(),
        bitmapInfo: CGBitmapInfo = .floatComponents
    ) -> CVPixelBuffer? {
        var maybePixelBuffer: CVPixelBuffer?
        let attrs = [
            kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue,
            kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue
        ]
        let status = CVPixelBufferCreate(
            kCFAllocatorDefault,
            width,
            height,
            pixelFormatType,
            attrs as CFDictionary,
            &maybePixelBuffer
        )

        guard status == kCVReturnSuccess, let pixelBuffer = maybePixelBuffer else {
            return nil
        }

        let flags = CVPixelBufferLockFlags(rawValue: 0)
        guard kCVReturnSuccess == CVPixelBufferLockBaseAddress(pixelBuffer, flags) else {
            return nil
        }
        defer { CVPixelBufferUnlockBaseAddress(pixelBuffer, flags) }

        var bitsPerComponent: Int?

        switch pixelFormatType {
        case kCVPixelFormatType_OneComponent8:
            bitsPerComponent = 8
        case kCVPixelFormatType_OneComponent16,
             kCVPixelFormatType_OneComponent16Half,
             kCVPixelFormatType_DepthFloat16,
             kCVPixelFormatType_16Gray:
            bitsPerComponent = 16
        case kCVPixelFormatType_OneComponent32Float, kCVPixelFormatType_DepthFloat32:
            bitsPerComponent = 32
        default:
            break
        }

        guard let bitsPerComponent = bitsPerComponent,
              let context = CGContext(
            data: CVPixelBufferGetBaseAddress(pixelBuffer),
            width: width,
            height: height,
            bitsPerComponent: bitsPerComponent,
            bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer),
            space: colorSpace,
            bitmapInfo: bitmapInfo.rawValue
        ) else {
            return nil
        }

        context.draw(self, in: CGRect(x: 0, y: 0, width: width, height: height))
        return pixelBuffer
    }
}

import CoreVideo

/// A Core Video related error
public struct CVError: Swift.Error, CustomStringConvertible {
    /// Readable failure context
    public let failure: String

    /// Core Video error type return value.
    public let cvReturnCode: CVReturn?

    init(_ failure: String, code cvReturnCode: CVReturn? = nil) {
        self.failure = failure
        self.cvReturnCode = cvReturnCode
    }

    public var description: String {
        if let cvReturnCode = cvReturnCode {
            return "\(failure) CVReturn=(\(cvReturnCode))"
        } else {
            return failure
        }
    }
}
